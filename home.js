// create a HTTP request when the page loads
let token;

document.getElementById("logout-button")?.addEventListener("click", function () {
  sessionStorage.clear();
  console.log("click!");
  location.href = "login.html";
});

window.onload = function () {
  const xhr = new XMLHttpRequest();
  xhr.open("GET", "http://3.133.113.250:8082/leaderboard");
  xhr.onreadystatechange = function () {
    if (xhr.readyState === 4 && xhr.status === 200) {
      console.log("successfully pulled leaderboard");
      renderLeaderboard(xhr.responseText);
    } else if (xhr.readyState === 4) {
      console.log("there was an issue with the request");
    }
  };
  xhr.send();

  token = sessionStorage.getItem("token");
  if (token) {


    // let logoutNav = document.getElementById("logout-nav")
    // let loginNav = document.getElementById("login-nav")
    // logoutNav.hidden = false;
    // loginNav.hidden = true;

    let user = document.getElementById("user");
    let username = token.substring(0, token.length - 11);
    // user.innerText = "User: " + username;

    // displayAccountBalance(token)

    const xhr = new XMLHttpRequest();
    let url = "http://3.133.113.250:8082/records?token=";
    url = url + token;
    xhr.open("GET", url);
    xhr.onreadystatechange = function () {
      if (xhr.readyState === 4 && xhr.status === 200) {
        console.log("successfully pulling balance");
        renderHistory(xhr.responseText);
      } else if (xhr.readyState === 4) {
        console.log("there was an issue with the request");
      }
    };
    xhr.send();

    let loginOnly = document.getElementsByClassName("account-only");
        for( const e of loginOnly){
            e.removeAttribute("hidden");
            // e.setAttribute("hidden", false)
            console.log("unhiding")
        }
    
    
        // let stranger = document.getElementsByClassName("no-account");
        // for( const e of stranger){
        //     e.setAttribute("hidden", true)
        //     console.log("hiding")
        // }
            
      

  }
  else{
    let notLoggedIn = document.getElementsByClassName("no-account");
    for(const e of notLoggedIn){
      e.removeAttribute("hidden");
      console.log('unhiding notloggedin');
    }
  }
  // else{
  //   let loginOnly = document.getElementsByClassName("account-only");
  //       for( const e of loginOnly){
  //           e.setAttribute("hidden", true)
  //           console.log("hiding")
  //       }
  //       let stranger = document.getElementsByClassName("no-account");
  //       for( const e of stranger){
  //           e.setAttribute("hidden", false)
  //           console.log("stranger loop")
  //       }
  // }
};

// let logoutNav = document.getElementById("logout-nav")
// // logoutNav.addEventListener("click", attemptLogout);
// let loginNav = document.getElementById("login-nav")

function attemptLogout() {
  if (token) {
    sessionStorage.removeItem("token");
    token = null;
    // logoutNav.hidden = true;
    // loginNav.hidden = false;
    alert("You've been logged out");
    window.location.href = "./home.html";
  } else {
    alert("Please log in first");

  }

};


function renderHistory(recordsJson) {
  const records = JSON.parse(recordsJson);
  const recordHtml = records
    .map(
      (record) =>
        `<li id="record-list-item-${record.id}" class="list-group-item"> amount: ${record.amount}  gameId: ${record.gameId} </li>`
    )
    .join("");
  document.getElementById("record-list").innerHTML = recordHtml;
};

// function renderLeaderboard(leaderBoardJson) {
//   const leaderBoard = JSON.parse(leaderBoardJson);
//   const leaderBoardHtml = leaderBoard
//     .map(
//       (account) =>
//         `<li id="record-list-item-${account.id}" class="list-group-item"> username: ${account.username}  balance: ${account.balance} </li>`
//     )
//     .join("");
//   document.getElementById("leaderboard-list").innerHTML = leaderBoardHtml;
// }

function renderLeaderboard(leaderBoardJson) {
  const tableHead = "<tr> <th>Username</th> <th>Total points</th></tr>";
  const leaderBoard = JSON.parse(leaderBoardJson);
  const leaderBoardHtml = leaderBoard
    .map(
      (account) =>
        `<tr> <td>${account.username}</td> <td>${account.balance}</td> </tr>`
    )
    .join("");
  document.getElementById("leaderboard-table").innerHTML = tableHead + leaderBoardHtml;
};


//   function GetAccountBalance(token){
//     var json = fetch(`http://localhost:8082/account?token=${token}`)
//         .then(res => res.json())
//         .then(function(json){
//             balance = json["balance"];
//             })
//         .catch(function(err){
//             console.log('fetch problem ' + err.message);
//         });
// }

// function displayAccountBalance(token){
//   const xhr = new XMLHttpRequest();
//   let url = "http://3.133.113.250:8082/account?token=";
//   url = url + token;
//   xhr.open("GET", url);
//   xhr.onreadystatechange = function () {
//     if (xhr.readyState === 4 && xhr.status === 200) {
//         console.log("success");
//         let account = JSON.parse(xhr.responseText);
//         console.log(account["balance"]);
//         let balance = account["balance"];
//         let prompt = document.getElementById("prompt");
//         console.log("balance:" + balance)
//         prompt.innerText = "You are free to play any games. You current have " + balance + " points";
//     } else if (xhr.readyState === 4) {
//         console.log("there was an issue with the request");
//     }
//   };
//   xhr.send();
// }