/******************************************
 * Account JS Created By Carlos Galvan Jr *
 *       for Revature Project 2           * 
 ******************************************/

/*************
*   SETUP    *
**************/
const API = "http://3.133.113.250:8082";
const games = ["Misc", "Rock Paper Scissors", "Stock Guessing Game", "Coin Flip Guessing Game"]
const gameUrl = ["Home", "./rockpaperscissors.html", "./stockguessing.html", "./coin-flip.html"]
let wins = 0;
let winsTotal = 0;
let losses = 0;
let lossesTotal = 0;
/**************
*   ONLOAD    *
**************/
window.onload = function () {
    const token = sessionStorage.getItem("token");
    if (token) {
        console.log("token provided" + token);
        getHistory()
    } else {
        //redirect elsewhere
        window.location.href = "./login.html";
        alert("Please log in First");
        console.log("no token provided");
    }
}
/********************
 *  XMLHTTPREQUEST  *
 ********************/
const getHistory = () => {
    const token = sessionStorage.getItem("token");
    if (token) {
        let user = document.getElementById("user");
        let username = token.substring(0, token.length - 11);
        user.innerText = username;
        updateTitle(username)
        const xhr = new XMLHttpRequest();
        let url = `${API}/records?token=`;
        url = url + token;
        xhr.open("GET", url);
        xhr.onreadystatechange = function () {
            if (xhr.readyState === 4 && xhr.status === 200) {
                console.log("success");

                renderHistory(xhr.responseText);
            } else if (xhr.readyState === 4) {
                console.log("there was an issue with the request");
            }
        };
        xhr.send();
    }
}

const GetAccountBalance = (callback) => {
    const token = sessionStorage.getItem("token");
    const json = fetch(`${API}/account?token=${token}`)
        .then(res => res.json())
        .then(callback)
        .catch(function (err) {
            console.log('fetch problem ' + err.message);
        });
}
/********************
 *  Generate Table  *
 ********************/

function renderHistory(recordsJson) {

    const records = JSON.parse(recordsJson);
    myChart(records)
    updateWinLossRatio(records)
    let count = 1
    if(records.length <= 0){
        let graphs = document.getElementsByClassName("visualize-data");
        for( let g of graphs){
            g.style.display = "none"
        }
        document.getElementById("empty-data").style.display = "block"
    }
    const recordHtml = records.reverse()
        .map(
            (record) => (parseInt(record.gameId) > 0 && parseInt(record.gameId) < 4) ?
                `
          <tr class="${getColor(parseInt(record.amount))} ${hiddenRowClassGenerator(count++)}">
          <td>${count - 1} </td>
            <td> <a href=${gameUrl[parseInt(record.gameId)]} class="link-secondary">${games[parseInt(record.gameId)]}</a></td>
            <td>${(parseInt(record.amount) > 0)
                    ? `+${record.amount}`
                    : record.amount
                }
                
            </td>
          </tr>

          `: ``

        )
        .join("");
    document.getElementById("record-list").innerHTML = recordHtml;


    if (count <= 10) {
        hideButton()
    }
}
const updateWinLossRatio = (records) => {
    for (const p of records) {
        if (parseInt(p.amount) > 0) {
            wins++
            winsTotal += parseInt(p.amount)
        } else if (parseInt(p.amount)) {
            losses ++
            lossesTotal += parseInt(p.amount)
        }
    }
    console.log("wins: " + wins);
    console.log("losses: " + losses)
    displayWinLoss()
}




/********************
 * CLASS NAME ADDER *
 ********************/
const getColor = (amount) => {
    if (amount > 0) {
        return "table-info win-plus"
    }
    else if (amount < 0) {
        return "table-danger win-minus"
    }
    else {
        return "table-default win-zero"
    }
}

const hiddenRowClassGenerator = (ct) => {

    if (ct <= 10) {
        return "initial-show"
    } else {
        return "initial-hide"
    }

}

/********************
 * DOM MANIPULATION *
 ********************/

const updateTitle = (un) => {
    document.getElementById("account-title").innerText = un;
}

const displayWinLoss = () => {
    document.getElementById("win-header").innerText = `${wins} games, ${winsTotal} points`;
    document.getElementById("loss-header").innerText = `${losses} games, ${lossesTotal} points`;
    GetAccountBalance(updateBalance);

}
const updateBalance = (json) => {
    const innertext = json.balance
    document.getElementById("balance-header").innerText = `${innertext} points`;
}
/********************
 *   BUTTON LOGIC   *
 ********************/

const viewMoreButton = () => {
    let rows = document.getElementsByClassName("initial-hide")

    for (const row of rows) {
        row.style.visibility = "visible"
    }
    hideButton();
}

const hideButton = () => {
    document.getElementById("view-more-button").style.display = "none"
}

/********************
 *  CHART.JS SETUP  *
 ********************/
let ctx = document.getElementById('myChart').getContext('2d');

const myChart = (recordArray) => {

    let rps = 0;
    let stg = 0;
    let cfgg = 0;
    for (const game of recordArray) {
        if (game.gameId == 1) {
            rps++
        } else if (game.gameId == 2) {
            stg++

        } else if (game.gameId == 3) {
            cfgg++
        }
    }
    let dataCount = [rps, stg, cfgg]

    new Chart(ctx, {
        type: 'doughnut',
        data: {
            labels: ["Rock Paper Scissors", "Stock Guessing Game", "Coin Flip Guessing Game"],
            datasets: [{
                label: 'My First Dataset',
                data: dataCount,
                backgroundColor: [
                    'rgb(255, 99, 132)',
                    'rgb(54, 162, 235)',
                    'rgb(255, 205, 86)'
                ],
                hoverOffset: 4
            }]
        },options: {
            plugins: {
                title: {
                    display: true,
                    text: 'Games Played'
                }
            }
        }
    })
}

/********************
 *  LOGOUT BUTTON   *
 ********************/

document.getElementById("logout-button")?.addEventListener("click", function () {
    sessionStorage.clear();
    console.log("click!");
    location.href = "login.html";
});

