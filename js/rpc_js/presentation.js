/**
  _______________    __  ___   _____
 /_  __/ ____/   |  /  |/  /  |__  /
  / / / __/ / /| | / /|_/ /    /_ < 
 / / / /___/ ___ |/ /  / /   ___/ / 
/_/ /_____/_/  |_/_/  /_/   /____/  
                                    
* This JS script was created by Carlos Galvan Jr for Team 3's Project 2

* Basic Funtionality: This script is used to handle rockpaperscissors.html game logic and general 
document manipulation. */

/*************
*   SETUP    *
**************/
const API = "http://3.133.113.250:8082/"; //EC2 IP + Docker/Springboot Port
let bet = 0;
let wagerBalance = 0;
let level = 1;
//Map for bg color \\ toggle
const bgColor = new Map();
bgColor.set("Rock", "bg-info")
bgColor.set("Paper", "bg-dark")
bgColor.set("Scissor", "bg-warning")


/**************
*   ONLOAD    *
**************/
window.onload = function () {
    const token = sessionStorage.getItem("token");
    if (token) {
        console.log("token provided" + token);
        GetAccountBalance(updateBalanceElement)

    } else {
        //redirect elsewhere
        window.location.href = "./login.html";
        alert("Please log in First");
        console.log("no token provided");
    }
}

/**************
 *   JQuery   *
 **************/

//Prevents form from reseting Page
$("#rock-paper-scissor-form").submit(function (e) {
    e.preventDefault();
});

//Bootstrap Popper Jquery 
var tooltipTriggerList = [].slice.call(document.querySelectorAll('[data-bs-toggle="tooltip"]'))
var tooltipList = tooltipTriggerList.map(function (tooltipTriggerEl) {
    return new bootstrap.Tooltip(tooltipTriggerEl)
})

/********************
 *  ACCOUNT DATA    *
 ********************/

const GetAccountBalance = (callback) => {
    const token = sessionStorage.getItem("token");
    const json = fetch(`${API}account?token=${token}`)
        .then(res => res.json())
        .then(callback)
        .catch(function (err) {
            console.log('fetch problem ' + err.message);
        });
}

const verifyToken = () => {
    const token = sessionStorage.getItem("token");
    if (token) {
        console.log("token provided");

    } else {
        //redirect elsewhere
        window.location.href = "./login.html";
        alert("Please log in First");
        console.log("no token provided");
    }

}

/********************
    GAME LOGIC
********************/
const getGuess = () => {
    showGameCard(false)
    loadingAnimation(true)
    var json = fetch(`${API}rockpaperscissor`)
        .then(res => res.json())
        .then(res => {
            console.log("The Server Chose: ")
            console.log(res)
            gameLogic(res)//starts game logic
        })
        .catch(function (err) {
            console.log('fetch problem ' + err.message);
        });
}

const gameLogic = (generatedGuess) => {
    bet = parseFloat(document.getElementById("money-input").value);
    evaluateWin(generatedGuess)
    level++
    updateWageBanner(wagerBalance)
    updateLevelBadge(level)
    GetAccountBalance(updateBalanceElement)
}

/**********************
    EVALUTATE GUESS
**********************/

const evaluateWin = (computerChoice) => {
    loadingAnimation(false)
    let userChoice = document.getElementsByName("rpc-check")
    for (var i = 0, length = userChoice.length; i < length; i++) {
        if (userChoice[i].checked) {
            userChoice = userChoice[i].value
            break;
        }
    }
    if (computerChoice.weakness === userChoice) {//win
        handleWin(userChoice, computerChoice.hand)
    } else if (computerChoice.effective === userChoice) {//loss
        handleLoss(userChoice, computerChoice.hand)
    } else if (computerChoice.hand === userChoice) {//tie
        handleTie(userChoice, computerChoice.hand)
    }
}

/***************************************
    HANDLE OUTCOME (WIN / LOSE / TIE)
***************************************/

const handleWin = (user, comp) => {
    // console.log("WIN")
    wagerBalance += bet;
    showEvalCard(true)
    GetAccountBalance(updateBalanceElement)
    console.log("wagerBalance: " + wagerBalance)
    document.getElementById("evaluate-win-container").setAttribute("class", "d-block")//show card body
    document.getElementById("game-eval-header").innerText = `Congrats! ${user} beats ${comp}` //set header
}
const handleLoss = (user, comp) => {
    // console.log("LOSS")
    wagerBalance = bet;
    createRecord(-wagerBalance)
    // wagerBalance = 0
    showEvalCard(true)
    GetAccountBalance(updateBalanceElement)
    document.getElementById("evaluate-loss-container").setAttribute("class", "d-block") //show card body
    document.getElementById("losing-eval-prompt").innerText = `You lost ${wagerBalance} points!` //show card bodyÒ
    document.getElementById("game-eval-header").innerText = `Loser! ${comp} beats ${user}`//set header

}
const handleTie = (user, comp) => {
    // console.log("TIE")
    wagerBalance -= bet;
    showEvalCard(true)
    if (wagerBalance > 0) {
        document.getElementById("evaluate-tie-container").setAttribute("class", "d-block") //show card body
        document.getElementById("game-eval-header").innerText = `Draw! You both chose ${user}`//set header
    } else {
        createRecord(wagerBalance)
        document.getElementById("evaluate-loss-container").setAttribute("class", "d-block") //show card bodyÒ
        document.getElementById("losing-eval-prompt").innerText = `Your losing bet exceeded your wager! You lost ${wagerBalance} points!` //show card bodyÒ
        document.getElementById("game-eval-header").innerText = `Game Over! You both chose ${user}`//set header
    }

}


/*************************************
    DOCUMENT BUTTONS Functionality
*************************************/

/*const radioButtonsSetters = () => {
    alert("changes")
    let radios = document.getElementsByName("rpc-check")
    radios.forEach(e => {
        console.log(e.value + " " + e.checked)
    });
}*/

const continueButton = () => {
    showEvalCard(false)
    showGameCard(true)
    clearChoice()
    bet = 0;
}

const playAgainButton = () => {
    location.reload();
}
const cashOut = async () => {
    await createRecord(wagerBalance)
    await GetAccountBalance(updateBalanceElement)
    document.getElementById("win-card-button-continue").disabled = true;
    document.getElementById("win-card-button-continue").style.display = "none";
    document.getElementById("win-card-button-cashout").disabled = true;
    document.getElementById("win-card-button-cashout").style.display = "none";
    document.getElementById("win-card-button-win-cont").style.display = "block";

    alert(`Good Game! ${wagerBalance} points has been added to your account`)
}

const quitButton = () =>{
    window.location.href = "./home.html";
}
/****************************
*    DISPLAY Manipulation   *
*****************************/
const showGameCard = (bool) => {
    if (bool) {
        document.getElementById("main-game-card").style.display = "block"
    } else {
        document.getElementById("main-game-card").style.display = "none"

    }
}

const showEvalCard = (bool) => {
    if (bool) {
        document.getElementById("game-eval-card").style.display = "block"
    } else {
        document.getElementById("game-eval-card").style.display = "none"
        document.getElementById("evaluate-win-container").setAttribute("class", "d-none")
        document.getElementById("evaluate-tie-container").setAttribute("class", "d-none")
        document.getElementById("evaluate-loss-container").setAttribute("class", "d-none")
        document.getElementById("game-eval-header").innerText = ""
    }
}

/****************************
    UPDATE SINGLE ELEMENTS
*****************************/

const updateLevelBadge = (lvl) => {
    document.getElementById("level-badge").innerText = `Lvl.${lvl}`
}

//callback user to update Balance
const updateBalanceElement = (json) => {
    const innertext = json.balance
    document.getElementById("computer-guess").innerText = `Points: ${innertext}`
    if(innertext > wagerBalance){
        document.getElementById("money-input").setAttribute("max", innertext)
    }else{
        document.getElementById("money-input").setAttribute("max", wagerBalance)
    }
    

    
}

const updateWageBanner = (points) => {
    document.getElementById("wager-balance").innerText = `Current Wager: ${points}`
}


/****************
      STYLES 
*****************/

const loadingAnimation = (bool) => {
    if (bool) {
        document.getElementById("loading-animation").style.display = "block";

    } else {
        document.getElementById("loading-animation").style.display = "none";
    }
}
//obsolite 
const changeBackground = () => {
    let radios = document.getElementsByName("rpc-check")
    radios.forEach(e => {
        if (e.checked) {
            document.getElementsByTagName("body")[0].setAttribute("class", bgColor.get(e.value))
            document.getElementById("rps-header").innerText = `You chose ${e.value}`
        }
    });

}

const changeBackgroundViaButton = (choice) => {
    document.getElementsByTagName("body")[0].setAttribute("class", bgColor.get(choice))
    document.getElementById("rps-header").innerHTML = `You chose: <h1 class="font-weight-bold inline">${choice} </h1>`
    document.getElementById("clear-container").setAttribute("class", "d-grid mb-2")
    document.getElementById("bet-area").setAttribute("class", "card-footer d-block")
}

/***********************
    RESET FUNCTIONS
************************/
const clearChoice = () => {
    console.log("Clearing Form")
    document.getElementById("rock-paper-scissor-form").reset();
    document.getElementsByTagName("body")[0].setAttribute("class", "bg-white")
    document.getElementById("rps-header").innerText = "Rock Paper Scissor?"
    document.getElementById("clear-container").setAttribute("class", "d-none")
    document.getElementById("bet-area").setAttribute("class", "card-footer d-none")
}

/************
*    SAVE   *
*************/

const createRecord = (amount) => {

    const token = sessionStorage.getItem("token");
    let requestBody = `gameId=${1}&amount=${amount}&token=${token}`;
    const xhr = new XMLHttpRequest();

    xhr.open("POST", `${API}records`);
    xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    xhr.onreadystatechange = function () {
        if (xhr.readyState === 4) {
            if (xhr.status === 201) {
                console.log("create Record: OK")
            } else {
                console.log("create Record: failed")
            }
        }
    }

    console.log(requestBody);
    xhr.send(requestBody);

}

