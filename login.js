window.onload = function () {
    const token = sessionStorage.getItem("token");
    console.log("checking token")
    if (token) {
        window.location.href = "./home.html";
        
    } 
} 

document.getElementById("login-form").addEventListener("submit", attemptLogin);

function attemptLogin(event){
    event.preventDefault();
    
    const username = document.getElementById("inputUsername").value;
    const password = document.getElementById("inputPassword").value;
    let requestBody = `username=${username}&password=${password}`;
    const xhr = new XMLHttpRequest();
    xhr.open("POST", "http://3.133.113.250:8082/login");
    xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    xhr.onreadystatechange = function () {
      if (xhr.readyState === 4) {
        if (xhr.status === 200) {
            loginSuccess(xhr);
        } else {
            loginFail();
        }
      }
    };
    console.log(requestBody);
    xhr.send(requestBody);
}

function loginSuccess(xhr){
    let token = xhr.getResponseHeader("Authorization");
    
    sessionStorage.setItem("token", token);
    console.log("token set");
    window.location.href = "./home.html";
}

function loginFail(){
    const errorDiv = document.getElementById("error-msg");
    console.log("login failed");
    errorDiv.hidden = false;

}
