let url = "http://3.133.113.250:8082/stockguessinggame";
let choice = undefined;
let winningCondition = undefined;

const modalTitle = document.getElementById("winOrLoseModalLabel");
const modalBody = document.getElementById("winOrLoseModalBody");
const gameId = 2;

var sOnePrice;
var sTwoPrice;
var token;
var balance;
var betAmount;
let registerA = document.getElementById("register-a");
let loginA = document.getElementById("login-a");
let logoutA = document.getElementById("logout-a");

document.getElementById("bet-input").addEventListener('keydown',function(e){if(e.keyIdentifier=='U+000A'||e.keyIdentifier=='Enter'||e.keyCode==13){if(e.target.nodeName=='INPUT'&&e.target.type=='number'){e.preventDefault();return false;}}},true);

window.onload = function () {
    getJson();
    token = sessionStorage.getItem("token");
    GetAccountBalance(token);
    if (token) {
        console.log("token provided");
    }else{
        //redirect elsewhere
        window.location.href = "./login.html";
        alert("Please log in First");
        console.log("no token provided");
    }
}

document.getElementById("logout-button").addEventListener("click",function(){
    sessionStorage.clear();
    console.log("click!");
    location.href = "login.html";
});

function GetAccountBalance(tkn){

    var json = fetch(`http://3.133.113.250:8082/account?token=${tkn}`)
        .then(res => res.json())
        .then(function(json){
                balance = json["balance"];
                document.getElementById("placeholder-balance").innerText = balance;
            })
        .catch(function(err){
            console.log('fetch problem ' + err.message);
        });
        
}

document.getElementById("confirm-bet-button").addEventListener("click",function(){

    var formVal = document.getElementById("bet-input").checkValidity();
    
    modalBody.innerText = "";
    if(winningCondition === undefined){
        modalTitle.innerText = "Error!";
        modalBody.innerText = "We're unable to process your input because we are missing information from our server. Please try again later";
    }else{
        if(!formVal && choice > 0){
            modalTitle.innerText = "Error: invalid bet";
            modalBody.innerText="Please input a bet amount that is at least 0";
        }else if(choice < 1 && formVal){
            modalTitle.innerText = "Error: invalid stock choice";
            modalBody.innerText="Please make a choice between the two stocks!"
        }else if(choice < 1 && !formVal){
            modalTitle.innerText = "Error: invalid bet and stock choice";
            modalBody.innerText="Please make a choice between the two stocks and input a valid bet!"
        }else if(choice > 0 && formVal){
            GetAccountBalance(token);
            console.log("choice "+  choice);
            console.log("formVal: " + document.getElementById("bet-input").value);
            betAmount = document.getElementById("bet-input").value;

            if(balance < betAmount){
                console.log("invalid bet amount")
                modalTitle.innerText = "Error: invalid bet";
                modalBody.innerText="Your bet must be equal to or less than your available balance";
            }else{

                if(document.getElementById("bet-input").nodeValue > balance){
                    console.log("invalid bet");
                }
    
                if(choice === winningCondition){
                    modalTitle.innerText="You Won!!";
                    createRecord(gameId, betAmount, token);
                    
                }else{
                    modalTitle.innerText="You Lost";
                    createRecord(gameId, 0 - betAmount, token);
                    
                }
                // GetAccountBalance(token);

                modalBody.innerText = (`Stock 1 Price: $${sOnePrice} | Stock 2 Price: $${sTwoPrice}`);
                getJson();
                
                
            }
            

            
        }
        
    }
    
})

// $("#exampleModal").on("shown.bs.modal",function(){
//     GetAccountBalance(token);
//     console.log("inside jquery");
// })

// $(document).on("click","#exampleModal",function(){
//     GetAccountBalance(token);
//     console.log("inside jquery");
// })

function decideGameOnChoice(choice){
    // const modalTitle = document.getElementById("winOrLoseModalLabel");
    document.getElementById("confirm-bet-button").setAttribute("data-bs-target","#winOrLoseModal");
    var formVal = document.getElementById("bet-input").checkValidity();
    console.log(formVal);
    // Add a post method to allow balance to be sent to database and received as new balance.
    if(formVal){
        if(choice === winningCondition){
            modalTitle.innerText="You Won!!";
        }else{
            modalTitle.innerText="You Lost";
        }
    }else{
        modalTitle.innerText="Please input a bet amount that is at least 0 and with only two decimal places."
    }
}

function setChoice(playerChoice){
    console.log("inside set choice")
    choice = playerChoice;

    console.log(choice);
}

document.getElementById("choice1").addEventListener("click",function(){
    choice = 1;
    console.log("choice: " + choice);
    
    decideGameOnChoice(choice);
});

document.getElementById("choice2").addEventListener("click",function(){
    choice = 2;
    console.log("choice: " + choice);

    decideGameOnChoice(choice);
});

function getJson(){
    fetch(url)
    .then(res => res.json())
    .then(
        function(json){
            var stockOne = json[0];
            var stockTwo = json[1];

            console.log(json);
            initializeButton1(stockOne,stockTwo);
            decideGame(stockOne,stockTwo);
        })
    .catch(function(err){
        console.log('Fetch problem: ' + err.message);
    });
}
function initializeButton1(button1, button2){
        
    let finalButton1;
    let finalButton2;

    finalButton1 = button1;
    finalButton2 = button2;
    updateButton(finalButton1,finalButton2);

    finalButton = [];
}

function updateButton(finalButton1,finalButton2){
    if(!finalButton1 && !finalButton2){
        document.getElementById("choice1").innerHTML = "loading";
        document.getElementById("choice2").innerHTML = "loading";
    }else{
        document.getElementById("choice1").innerHTML = finalButton1["symbol"];
        document.getElementById("choice2").innerHTML = finalButton2["symbol"];
    }
}

let x = 12;

function decideGame(stockOneJson,stockTwoJson){
    console.log(stockOneJson["price"]);
    console.log(stockTwoJson["price"]);

    sOnePrice = stockOneJson["price"];
    sTwoPrice = stockTwoJson["price"];

    console.log("choice: " + choice);
    if(sOnePrice > sTwoPrice){
        winningCondition = 1;
    }else{
        winningCondition = 2;
    }
    console.log("win con:" + winningCondition);

}

function createRecord(gameId, amount, token){
    
    
    let requestBody = `gameId=${gameId}&amount=${amount}&token=${token}`;
    const xhr = new XMLHttpRequest();
    
    xhr.open("POST", "http://3.133.113.250:8082/records");
    xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    xhr.onreadystatechange = function () {
        if (xhr.readyState === 4) {
            if (xhr.status === 201) {
            console.log("create Record: OK")
            GetAccountBalance(token)
            } else {
            console.log("create Record: failed")
            }
        }
    }

    console.log(requestBody);
    xhr.send(requestBody);

}