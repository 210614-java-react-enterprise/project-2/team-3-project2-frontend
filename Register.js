let validForm = false

window.onload = function () {
  const token = sessionStorage.getItem("token");
  console.log("checking token")
  if (token) {
    window.location.href = "./home.html";

  }
}

document.getElementById("register-form").addEventListener("submit", attemptRegister);

function attemptRegister(event) {
  if(validForm){
    indicateProcess();
  event.preventDefault();

  
  const email = document.getElementById("inputEmail").value;
  const username = document.getElementById("inputUsername").value;
  const password = document.getElementById("inputPassword").value;

  const account = { email, username, password };

  // stringify account object
  const accountJson = JSON.stringify(account);


  //const token = sessionStorage.getItem("token");
  //if (token) {
  const xhr = new XMLHttpRequest();
  xhr.open("POST", "http://3.133.113.250:8082/register");
  //xhr.setRequestHeader("Authorization", "token");
  xhr.setRequestHeader("Content-Type", "application/json");
  xhr.onreadystatechange = function () {
    if (xhr.readyState === 4) {
      if (xhr.status === 201) {
        indicateSuccess();
      } else {
        indicateFailure();
      }
    }
  };
  console.log(accountJson);
  xhr.send(accountJson);
}else{
  alert("Please Fill Out The Entire Form Please")
}
}

function indicateProcess() {
  indicateResult("creating an account...");
}
function indicateSuccess() {
  indicateResult("Successfully created an account");
}

function indicateFailure() {
  indicateResult("There was an issue creating your account. Please try with another usename");
}

function indicateResult(message) {
  const messageDiv = document.getElementById("register-msg");
  messageDiv.hidden = false;
  messageDiv.innerText = message;
}

/* $("input#inputUsername").bind({
  keydown: function (e) {
    let key = e.keyCode || e.which;

    if (e.which === 32){
      return false;
    } 
    else if (String.fromCharCode(key).match("^(?=.{8,20}$)(?![_.])(?!.*[_.]{2})[a-zA-Z0-9._]+(?<![_.])$")) {
      console.log(String.fromCharCode(key))
      return true;
    }
  },
  change: function () {
    console.log(this.value)
    this.value = ""
  }
}); */
//validates username and adds warning
$(function () {

  jQuery('#inputUsername').bind('blur', function () {

    // $("#inputUsername").removeClass();

    let username = $(this).val();
    let isValid = /^[a-zA-Z0-9]*$/.test(username);
    console.log(isValid + " " + username)
    let length = username.length;
    if (isValid && (length > 7) && (length < 21)) {
      $("#inputUsername").removeClass();
      $("#inputUsername").addClass("form-control");
      $("#inputUsername").addClass("is-valid");
      $("submit-btn").prop("disabled",false);
      validForm = true;
    } else {
      $("#inputUsername").removeClass();
      $("#inputUsername").addClass("form-control");
      $("#inputUsername").addClass("is-invalid");
      validForm = false;
      // $("submit-btn").prop("disabled",true);
    }
  });
});

$(function () {

  jQuery('#inputPassword').bind('blur', function () {

    // $("#inputUsername").removeClass();

    let password = $(this).val();
    let isValid = /^[a-zA-Z0-9]*$/.test(password);
    let length = password.length;
    if (isValid && (length > 7) && (length < 17)) {
      $("#inputPassword").removeClass();
      $("#inputPassword").addClass("form-control");
      $("#inputPassword").addClass("is-valid");
      validForm = true;
      // $("submit-btn").prop("disabled",false);
    } else {
      $("#inputPassword").removeClass();
      $("#inputPassword").addClass("form-control");
      $("#inputPassword").addClass("is-invalid");
      validForm = false;
      // $("submit-btn").prop("disabled",true);
    }
  });
});


