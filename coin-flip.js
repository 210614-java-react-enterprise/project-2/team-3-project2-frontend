let betAmount;
const betMsg = document.getElementById("bet-msg");

window.onload = function () {
    const token = sessionStorage.getItem("token");
    if (token) {
        console.log("token provided");
    }else{
        //redirect elsewhere
        window.location.href = "./login.html";
        alert("Please log in First");
        console.log("no token provided");
    }
}


document.getElementById("play-button").addEventListener("click", play);

function play(event){
    const token = sessionStorage.getItem("token");
    
    if (token) {
        betAmount = document.getElementById("betAmount").value;
        isValid = document.getElementById("betAmount").checkValidity()
        if(betAmount && betAmount > 0){
            console.log(betAmount);
            console.log(Number.isInteger(betAmount));
            if(isValid){             
                betMsg.innerText = `Your bet amount: ${betAmount} points  `
                betMsg.hidden = false;
            }else{
                alert("Please enter an interger for bet amount");
                betAmount = 0;
            }

        }else{
            alert("Please enter valid bet");
        }
        

    }else {
        //redirect elsewhere
        window.location.href = "./login.html";
        alert("Please log in First");
        console.log("no token provided");
    }
}

document.getElementById("heads-button").addEventListener("click", processCoinFlip);
document.getElementById("tails-button").addEventListener("click", processCoinFlip);

//document.getElementById("confirm-button").addEventListener("click", processCoinFlip);

function processCoinFlip(event){
    const token = sessionStorage.getItem("token");
    if (betAmount) {
        console.log(betAmount);
        let random = Math.floor(Math.random() * 2);
        console.log("game start " + random);
        console.log(event);
        //const guess = document.getElementById("guess-number").value;
        const guess = event.target.value;

        console.log("guess: " + guess);
        const resultMsg = document.getElementById("result-msg");
        if(random==guess){
            console.log("sending record");
            resultMsg.innerText = "you guessed it. You earned " + betAmount + " points";
            resultMsg.hidden = false;
            createRecord(3, betAmount, token)
        }else{
            console.log("sending record");
            resultMsg.innerText = "wrong guess. You lost " + betAmount + " points";
            resultMsg.hidden = false;
            createRecord(3, 0 - betAmount, token)
        }
        betAmount = 0;
        betMsg.hidden = true;
    }else {
        alert("Please enter valid bet");

    }

}

function createRecord(gameId, amount, token){
    
    let requestBody = `gameId=${gameId}&amount=${amount}&token=${token}`;
    const xhr = new XMLHttpRequest();
    
    xhr.open("POST", "http://3.133.113.250:8082/records");
    xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    xhr.onreadystatechange = function () {
        if (xhr.readyState === 4) {
            if (xhr.status === 201) {
            console.log("create Record: OK")
            } else {
            console.log("create Record: failed")
            }
        }
    }

    console.log(requestBody);
    xhr.send(requestBody);

}


